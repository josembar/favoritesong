/*
 * This is an exercise regarding Java variables.
 * I, Jose B, created this simple project in order to ilustrate how 
 * variables are declared and used in Java. For this I will use information
 * of my favorite song. Please enjoy :)
 */
package favoritesong;

public class FavoriteSong {

    public static void main(String[] args) {
        //My favorite song's info
        
        //name
        String songName = "You Know My Name";
        //author
        String songAuthor = "Chris Cornell";
        //performer
        String songPerformer = "Chris Cornell";
        //year of recording
        int songYearRecorded = 2006;
        //year of release
        int songYearReleased = 2006;
        //genre
        String songGenre = "Alternative rock";
        //length
        double songLength = 4.02;
        //available in Spotify Y/N
        char songAvailableOnSpotify = 'Y';

        //printing the info
        System.out.println("Song's info:");
        System.out.println("Name: " + songName);
        System.out.println("Author: " + songAuthor);
        System.out.println("Performed by: " + songPerformer);
        System.out.println("Recorded in: " + songYearRecorded);
        System.out.println("Released in: " + songYearReleased);
        System.out.println("Genre: " + songGenre);
        System.out.println("Length: " + songLength);
        System.out.println("Available on Spotify: " + songAvailableOnSpotify);
    }
    
}
